<title>FF XIII-2 clock puzzle solver</title>
<h1>FF XIII-2 clock puzzle solver</h1>
<form action="solve.php">
好きなところから時計回りに全ての数字をつなげたものを打ち込んでください<br/>
<input type="text" name="arg">
<input type="submit">
</form>
<?php
$arg = $_REQUEST['arg']; // inputされた文字列
if (strlen($arg) > 0){
	$val = array(); // 各数字を入れた配列
	$status = array(); // すでに踏んでる=1、まだ=0
	// 初期化
	for($i=0; $i < strlen($arg); $i++){
		$num = substr($arg, $i, 1);
		if (is_numeric($num)){
			$val[] = $num;
			echo moji($val, $i);
			$status[] = 0;
		}else{
			echo '半角数字だけいれてちょ';
			exit();
		}
	}
	// 各スタート位置からsolve functionを使う
	$all_answers = array();
	for($start = 0; $start < count($val); $start++){
		$tmp_status = $status;
		$tmp_status[$start] = 1; // start地点のstatusを変更
		$answers = solve($start, $val, $tmp_status); 
		$all_answers = array_merge($all_answers, $answers);
	}
	// 表示
	if(count($all_answers) == 0){
		echo 'は解けません…';
	}else{
		echo 'の解き方は'.count($all_answers).'通りあります<br/>';
		foreach($all_answers as $answer_arr){
			$for_show = array();
			// positionでの答えをmoji化
			foreach ($answer_arr as $key => $position){
				$for_show[$key] = moji($val, $position);
			}
			// moji化した答えを整形して表示
			$answer = implode("→",array_reverse($for_show));
			echo $answer."<br/>";
		}
	}
}else{
	// 入力がない場合
	echo '入力どうぞ';
}

function solve($position, $val, $status){
	$answer = array();
	$val_len = count($val);
	// 時計回りに進んだ場合
	$new_position_plus = ($position + $val[$position]) % $val_len;
	$answer_plus = next_step($new_position_plus, $val, $status);
	// 反時計回りに進んだ場合
	$new_position_minus = ($position - $val[$position] + $val_len * 10) % $val_len;
	$answer_minus = next_step($new_position_minus, $val, $status);
	$answer =array_merge($answer_plus, $answer_minus);
	// 現在のpositionを先から返ってきた答えに加えてリターン
	foreach($answer as $key => $value){
		$answer[$key][] = $position;
	}
	return $answer;
}

function next_step($new_position, $val, $status){
	$answer = array();
	if($status[$new_position] == 1){
		// new_positionはすでに踏んでるので調査終了
		//$answer[][0] = 999;
	}else{
		$status[$new_position] = 1;
		if(!in_array(0, $status)){
			// new_positionでゴール
			$answer[][0] = $new_position;
		}
		// new_positionではゴールも終了もしないで調査続行
		$answer = array_merge($answer, solve($new_position, $val, $status));
	}
	return $answer;
}

function moji($val,$position){
	return "<span style='font-size:40pt'>".$val[$position]."</span>(=".chr(65+$position).") ";
}
?>
